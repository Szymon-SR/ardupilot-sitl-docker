# What's this?

This repository packages ArduPilot's simulator (SITL) into
a ready-to-use docker container. It also provides
a std-only Python script to set all the complicated docker &
sitl flags for the user to make the whole procedure a bit easier for newcomers.


## Glossary

[Ardupilot](http://ardupilot.org/) - open-source autopilot for unmanned vehicles (drones, planes, boats, etc)

[SITL](http://ardupilot.org/dev/docs/sitl-simulator-software-in-the-loop.html) - software in the loop - a simulator for ardupilot


## Usage

### Requirements

- `docker` installed on your system
- `python` installed on your system (at least version 3.7)

### `sitl.py` script

The script is documented, so I'll just post the output of `./sitl.py --help` here:

```
usage: sitl.py [-h] [-m] [-c] -v VEHICLE [-l LOCATION]

SITL Runner

optional arguments:
  -h, --help            show this help message and exit
  -m, --map             Show the map window
  -c, --console         Show the console window
  -v VEHICLE, --vehicle VEHICLE
                        Choose vehicle (ArduPlane or ArduCopter)
  -l LOCATION, --location LOCATION
                        Select location (Legnica by default)
```


### Manually, through `docker`

Docker-compose example [here](https://github.com/Wint3rmute/ardupilot-sitl-docker/blob/master/docker-compose.yml)

Alternatively, here's your long boi for running the container from the command line:

`docker run -it --net=host --env="DISPLAY" --volume="$HOME/.Xauthority:/home/akl/.Xauthority:rw" wnt3rmute/ardupilot-sitl ./sim_vehicle.py -L Ballarat --console --map -v ArduCopter -N`


# How X forwarding works

GUI works through sharing the `.Xauthority` file (see tutorials on how to use host's X from inside Docker. 
Wayland works through XWayland (last time that I checked).
