from sitl import SitlDockerHelper
from pymavlink import mavutil


def test_container_connectivity():
    sitl = SitlDockerHelper("ArduCopter", run_in_background=True)
    sitl.run()

    connection = mavutil.mavlink_connection("udpin:0.0.0.0:14551")
    connection.wait_heartbeat()

    while True:
        msg = connection.recv_match(blocking=True)
        if msg.get_type() == "GPS_RAW_INT":
            lng = float(msg.lon) * 10e-8
            lat = float(msg.lat) * 10e-8

            print("GOT GPS:", lat, lng)
            break

    sitl.stop()
    pass


if __name__ == "__main__":
    test_container_connectivity()